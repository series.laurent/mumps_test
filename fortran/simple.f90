program mumps_test

  implicit none
  include 'mpif.h'
  include 'dmumps_struc.h'
 
  type (dmumps_struc) mumps_par
  integer ierr, i
  integer(8) i8
 
  call mpi_init(ierr)

  !define a communicator for the package.
  mumps_par%comm = mpi_comm_world

  !initialize an instance of the package
  !for lu factorization (sym = 0, with working host)
  mumps_par%job = -1
  mumps_par%sym = 0
  mumps_par%par = 1
  call dmumps(mumps_par)
  if (mumps_par%infog(1).lt.0) then
     print *,  " error return: "
     print *,  "  mumps_par%infog(1)= ", mumps_par%infog(1)
     print *,  "  mumps_par%infog(2)= ", mumps_par%infog(2)
     call mpi_finalize(ierr)
     call exit(1)
  end if
  
  !define problem on the host (processor 0)
  if ( mumps_par%myid .eq. 0 ) then
    read(5,*) mumps_par%n
    read(5,*) mumps_par%nz
    allocate( mumps_par%irn ( mumps_par%nz ) )
    allocate( mumps_par%jcn ( mumps_par%nz ) )
    allocate( mumps_par%a( mumps_par%nz ) )
    allocate( mumps_par%rhs ( mumps_par%n  ) )
    do i8 = 1, mumps_par%nz
      read(5,*) mumps_par%irn(i8),mumps_par%jcn(i8),mumps_par%a(i8)
    end do
    do i = 1, mumps_par%n
      read(5,*) mumps_par%rhs(i)
    end do
  end if

  !call package for solution
  mumps_par%job = 6
  call dmumps(mumps_par)
  if (mumps_par%infog(1).lt.0) then
     print *,  " error return: "
     print *,  "  mumps_par%infog(1)= ", mumps_par%infog(1)
     print *,  "  mumps_par%infog(2)= ", mumps_par%infog(2)
     call mpi_finalize(ierr)
     call exit(1)
  end if

  !solution has been assembled on the host
  if ( mumps_par%myid .eq. 0 ) then
    write( 6, * ) ' solution is ',(mumps_par%rhs(i),i=1,mumps_par%n)
  end if

  !deallocate user data
  if ( mumps_par%myid .eq. 0 )then
    deallocate( mumps_par%irn )
    deallocate( mumps_par%jcn )
    deallocate( mumps_par%a   )
    deallocate( mumps_par%rhs )
  end if

  !destroy the instance (deallocate internal data structures)
  mumps_par%job = -2
  call dmumps(mumps_par)
  if (mumps_par%infog(1).lt.0) then
     print *,  " error return: "
     print *,  "  mumps_par%infog(1)= ", mumps_par%infog(1)
     print *,  "  mumps_par%infog(2)= ", mumps_par%infog(2)
     call mpi_finalize(ierr)
     call exit(1)
  end if

  call mpi_finalize(ierr)

end program
